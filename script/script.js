$(document).ready(function() {
    let pro = [
        pro1 = {
            image: "pro-white-2019.jpeg",
            model: "MacBook Pro 2019",
            price: "2700$",
            color: "white"
        },
        pro2 = {
            image: "pro-grey-2019.jpeg",
            model: "MacBook Pro 2019",
            price: "2700$",
            color: "grey"
        },
        pro3 = {
            image: "pro-white-2019.jpeg",
            model: "MacBook Pro 2018",
            price: "2500$",
            color: "white"
        },
        pro4 = {
            image: "pro-grey-2019.jpeg",
            model: "MacBook Pro 2018",
            price: "2500$",
            color: "grey"
        },
        pro5 = {
            image: "pro-13-white.jpeg",
            model: "MacBookPro 2019 13",
            price: "2000$",
            color: "white"
        },
        pro6 = {
            image: "pro-13-grey.jpeg",
            model: "MacBookPro 2019 13",
            price: "2000$",
            color: "grey"
        },
        pro7 = {
            image: "pro-13-white.jpeg",
            model: "MacBookPro 2018 13",
            price: "1850$",
            color: "white"
        },
        pro8 = {
            image: "pro-13-grey.jpeg",
            model: "MacBookPro 2018 13",
            price: "1850$",
            color: "grey"

        }
    ]
    let Air = [
        Air1 = {
            image: "air1.jpeg",
            model: "MacBook Air 2019",
            price: "1500$"
        },
        Air2 = {
            image: "air2.jpeg",
            model: "MacBook Air 2019",
            price: "1500$"
        },
        Air3 = {
            image: "air3.jpeg",
            model: "MacBook Air 2019",
            price: "1500$"
        },
        Air4 = {
            image: "air1.jpeg",
            model: "MacBook Air 2018",
            price: "1200$"
        },
        Air5 = {
            image: "air2.jpeg",
            model: "MacBook Air 2018",
            price: "1200$"
        },
        Air6 = {
            image: "air3.jpeg",
            model: "MacBook Air 2018",
            price: "1200$"
        },
        Air7 = {
            image: "air1.jpeg",
            model: "MacBook Air 2017",
            price: "1000$"
        },
        Air8 = {
            image: "air2.jpeg",
            model: "MacBook Air 2017",
            price: "1000$"
        }
    ]

    let iMac = [
        iMac1 = {
            image: "imac1.jpeg",
            model: "iMac 2019",
            price: "1100$"
        },
        iMac2 = {
            image: "imac2.jpeg",
            model: "iMac 2019",
            price: "1100$"
        },
        iMac3 = {
            image: "imac3.jpeg",
            model: "iMac 2018",
            price: "1100$"
        },
        iMac4 = {
            image: "imac1.jpeg",
            model: "iMac 2018",
            price: "1100$"
        },
        iMac5 = {
            image: "imac2.jpeg",
            model: "iMac 2017",
            price: "1100$"
        },
        iMac6 = {
            image: "imac3.jpeg",
            model: "iMac 2017",
            price: "1100$"
        },
        iMac7 = {
            image: "imac1.jpeg",
            model: "iMac 2016",
            price: "1100$"
        },
        iMac8 = {
            image: "imac2.jpeg",
            model: "iMac 2016",
            price: "1100$"
        }
    ]

    function descriptionfade() {
        $(".card").hover(function(e) {
            console.log(e);
            $(e["currentTarget"].children["2"]).fadeIn();
        });
        $(".card").mouseleave(function() {
            $(".hover").slideUp();
        });
    }

    function loopcontent() {
        for (let i = 0; i <= 7; i++) {
            var macPro = ` 
            <div class="col-lg-3 col-md-6 col-sm-6">
                 <div class="card">
                <img src="image/mac/${pro[i].image}" class="card-img-top" alt="laptop picture">
                <div class="card-body text-center">
                    <h5 class="card-title">${pro[i].model}</h5>
                    <p class="card-text">Price : ${pro[i].price}</p>
                </div>
                <div class="hover">
                    <ul>
                        <li>${pro[i].model}</li>
                        <li>Touch Bar : Yes</li>
                        <li>Color : ${pro[i].color}</li>
                        <li>Processor : 2.3GHz quad-core Intel Core i5, Turbo Boost up to 3.8GHz, with 128MB of eDRAM</li>
                        <li>Storage : 250GB</li>
                        <li>Memory : 16GB</li>
                    </ul>
                </div>
              </div>
               
            </div>`
            $(".loopMac").append(macPro);

            var macAir = ` <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="card">
                                    <img src="image/mac/macAir/${Air[i].image}" class="card-img-top" alt="laptop picture">
                                    <div class="card-body text-center">
                                        <h5 class="card-title">${Air[i].model}</h5>
                                        <p class="card-text">Price : ${Air[i].price}</p>
                                    </div>
                                    <div class="hover">
                                        <ul>
                                            <li>${Air[i].model}</li>
                                            <li>Processor : 2.3GHz quad-core Intel Core i5, Turbo Boost up to 3.8GHz, with 128MB of eDRAM</li>
                                            <li>Storage : 250GB</li>
                                            <li>Memory : 16GB</li>
                                        </ul>
                                     </div>
                                </div>                                        
                            </div>`
            $(".loopMacAir").append(macAir);

            var imac = ` <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card">
                                <img src="image/mac/imac/${iMac[i].image}" class="card-img-top" alt="laptop picture">
                                <div class="card-body text-center">
                                    <h5 class="card-title">${iMac[i].model}</h5>
                                    <p class="card-text">Price : ${iMac[i].price}</p>
                                </div>
                                <div class="hover">
                                    <ul>
                                        <li>${iMac[i].model}</li>
                                        <li>Processor : 2.3GHz Dual-Core Processor with Turbo Boost up to 3.6GHz
                                        1TB Storage</li>
                                        <li>Memory : 16GB</li>
                                    </ul>
                                </div>
                            </div>
                        </div>`
            $(".loopIMac").append(imac);
        }


    }
    loopcontent()
    descriptionfade()

    // ..............scroll top function...........
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() { scrollFunction() };

    function scrollFunction() {
        if (document.documentElement.scrollTop > 50) {
            document.getElementById("btnScroll").style.display = "block";

        } else {
            document.getElementById("btnScroll").style.display = "none";

        }
    }

    $('#btnScroll').click(function() {
        $("html,body").animate({
            scrollTop: 0
        }, 1000);
    });

    $('#toMacPro').click(function() {
        $('html, body').animate({
            scrollTop: $("#MacPro").offset().top
        }, 1000);
    });
    $('#toMacAir').click(function() {
        $('html, body').animate({
            scrollTop: $("#MacAir").offset().top
        }, 1000);
    });
    $('#toiMac').click(function() {
        $('html, body').animate({
            scrollTop: $("#iMac").offset().top
        }, 1000);
    });

    $('#sub-comment').click(function() {

        var email = $('#email').val();
        var comment = $('#comment').val();

        var feedback = `<div class="ufeedback">
                            <hr>
                            <div class="user">
                                <p><i class="fas fa-user"></i> &nbsp; ${email}</p>
                            </div>
                            <div class="ucomment">
                                <p>${comment}</p>
                            </div>
                        </div>`
        $('.feedback').append(feedback)

    })


})